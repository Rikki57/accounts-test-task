package ru.mvbiryukov.accountsclient.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.mvbiryukov.accountsclient.integration.DataProvider;
import ru.mvbiryukov.accountsclient.service.dto.Account;
import ru.mvbiryukov.accountsclient.service.dto.Operation;
import ru.mvbiryukov.accountsclient.service.dto.Owner;
import ru.mvbiryukov.accountsclient.service.dto.TransferResult;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class AccountServiceTest {
    private final static String TEST_ACCOUNT_NUMBER = "11111";
    private final static String TEST_RECEIVER_NUMBER = "22222";
    private final static String TEST_OWNER_NAME = "Ivan Ivanov";
    private final static Double TEST_BALANCE = 111.11;
    @Autowired
    private AccountService accountService;
    @MockBean
    private DataProvider dataProvider;

    @Before
    public void setUp() {
        Account account = new Account(TEST_ACCOUNT_NUMBER, new Owner(TEST_OWNER_NAME), TEST_BALANCE);
        Operation operation = new Operation(TEST_ACCOUNT_NUMBER, TEST_RECEIVER_NUMBER, TEST_BALANCE);
        List<Operation> operations = Arrays.asList(operation);
        TransferResult transferResult = new TransferResult(0, TEST_BALANCE, TEST_RECEIVER_NUMBER);

        Mockito.when(dataProvider.getAccountInfo(TEST_ACCOUNT_NUMBER))
                .thenReturn(account);
        Mockito.when(dataProvider.getOperationsList(TEST_ACCOUNT_NUMBER))
                .thenReturn(operations);
        Mockito.when(dataProvider.makeTransfer(any()))
                .thenReturn(transferResult);
    }

    @Test
    public void testGetAccountInfo() {
        Account account = accountService.getAccountInfo(TEST_ACCOUNT_NUMBER);
        assertThat(account.getAccountNumber()).isEqualTo(TEST_ACCOUNT_NUMBER);
        assertThat(account.getOwner().getName()).isEqualTo(TEST_OWNER_NAME);
    }

    @Test
    public void testGetOperations() {
        List<Operation> operation = accountService.getOperations(TEST_ACCOUNT_NUMBER);
        assertThat(operation.get(0).getAccountFrom()).isEqualTo(TEST_ACCOUNT_NUMBER);
        assertThat(operation.get(0).getAccountTo()).isEqualTo(TEST_RECEIVER_NUMBER);
    }

    @Test
    public void testMakeTransfer() {
        TransferResult result = accountService.makeTransfer(TEST_ACCOUNT_NUMBER, TEST_RECEIVER_NUMBER, TEST_BALANCE);
        assertThat(result.getAccountTarget()).isEqualTo(TEST_RECEIVER_NUMBER);
        assertThat(result.getAmount()).isEqualTo(TEST_BALANCE);
        assertThat(result.getStatus()).isEqualTo(0);
    }

    @TestConfiguration
    static class AccountServiceImplTestContextConfiguration {
        @Bean
        public AccountService accountService() {
            return new AccountService();
        }
    }

}
