package ru.mvbiryukov.accountsclient.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.mvbiryukov.accountsclient.service.AccountService;
import ru.mvbiryukov.accountsclient.service.dto.Account;
import ru.mvbiryukov.accountsclient.service.dto.Operation;
import ru.mvbiryukov.accountsclient.service.dto.Owner;
import ru.mvbiryukov.accountsclient.service.dto.TransferResult;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {
    private final static String TEST_ACCOUNT_NUMBER = "11111";
    private final static String TEST_OWNER_NAME = "Ivan Ivanov";
    private final static Double TEST_BALANCE = 111.11;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService service;

    @Test
    public void testSearch() throws Exception {
        mvc.perform(get("/searchAccount"))
                .andExpect(status().isOk())
                .andExpect(view().name("search"));
    }

    @Test
    public void testGetAccount() throws Exception {
        Account account = new Account(TEST_ACCOUNT_NUMBER, new Owner(TEST_OWNER_NAME), TEST_BALANCE);
        given(service.getAccountInfo(TEST_ACCOUNT_NUMBER)).willReturn(account);

        mvc.perform(post("/getAccount").param("accountNumber", TEST_ACCOUNT_NUMBER))
                .andExpect(status().isOk())
                .andExpect(view().name("account"))
                .andExpect(model().attribute("account", equalTo(account)));
    }

    @Test
    public void testShowOperations() throws Exception {
        Account account = new Account(TEST_ACCOUNT_NUMBER, new Owner(TEST_OWNER_NAME), TEST_BALANCE);
        Operation operation = new Operation(TEST_ACCOUNT_NUMBER, TEST_ACCOUNT_NUMBER, TEST_BALANCE);
        List<Operation> operations = Arrays.asList(operation);

        given(service.getAccountInfo(TEST_ACCOUNT_NUMBER)).willReturn(account);
        given(service.getOperations(TEST_ACCOUNT_NUMBER)).willReturn(operations);

        mvc.perform(get("/showOperations").param("accountNumber", TEST_ACCOUNT_NUMBER))
                .andExpect(status().isOk())
                .andExpect(view().name("operations"))
                .andExpect(model().attribute("account", equalTo(account)))
                .andExpect(model().attribute("operations", equalTo(operations)));
    }

    @Test
    public void testMakeTransfer() throws Exception {
        TransferResult transferResult = new TransferResult(0, TEST_BALANCE, TEST_ACCOUNT_NUMBER);
        Account account = new Account(TEST_ACCOUNT_NUMBER, new Owner(TEST_OWNER_NAME), TEST_BALANCE);

        given(service.makeTransfer(TEST_ACCOUNT_NUMBER, TEST_ACCOUNT_NUMBER, TEST_BALANCE)).willReturn(transferResult);
        given(service.getAccountInfo(TEST_ACCOUNT_NUMBER)).willReturn(account);

        mvc.perform(post("/makeTransfer")
                .param("accountNumber", TEST_ACCOUNT_NUMBER)
                .param("accountTarget", TEST_ACCOUNT_NUMBER)
                .param("amount", String.valueOf(TEST_BALANCE)))
                .andExpect(status().isOk())
                .andExpect(view().name("account"))
                .andExpect(model().attribute("account", equalTo(account)))
                .andExpect(model().attribute("transferResult", equalTo(transferResult)));
    }
}

