package ru.mvbiryukov.accountsclient.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.mvbiryukov.accountsclient.integration.jms.JmsReceiver;
import ru.mvbiryukov.accountsclient.integration.jms.JmsSender;
import ru.mvbiryukov.accountsclient.service.dto.*;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@RunWith(SpringRunner.class)
public class DataProviderTest {
    private final static String TEST_ACCOUNT_NUMBER = "11111";
    private final static String TEST_RECEIVER_NUMBER = "22222";
    private final static String TEST_OWNER_NAME = "Ivan Ivanov";
    private final static Double TEST_BALANCE = 111.11;
    private static final String ACCOUNT_RESPONSE_QUEUE = "accountRequest.out";
    private static final String OPERATIONS_RESPONSE_QUEUE = "operationsRequest.out";
    private static final String TRANSFER_RESPONSE_QUEUE = "transferRequest.out";
    private static final String TEST_ACCOUNT_RESPONSE =
            "{\"accountNumber\":\"11111\",\"owner\":{\"name\":\"Ivan Ivanov\"},\"balance\":111.11}";
    private static final String TEST_OPERATIONS_RESPONSE =
            "[{\"accountFrom\":\"" + TEST_ACCOUNT_NUMBER + "\",\"accountTo\":\"" + TEST_RECEIVER_NUMBER + "\",\"summ\":" + TEST_BALANCE +
                    "},{\"accountFrom\":\"" + TEST_ACCOUNT_NUMBER + "\",\"accountTo\":\"" + TEST_RECEIVER_NUMBER + "\",\"summ\":" + TEST_BALANCE + "}]";
    private static final String TEST_TRANSFER_RESPONSE = "{\"status\":0,\"amount\":" + TEST_BALANCE +
            ",\"accountTarget\":\"" + TEST_RECEIVER_NUMBER + "\"}";

    @MockBean
    private JmsReceiver jmsReceiver;

    @MockBean
    private JmsSender jmsSender;
    @Autowired
    private DataProvider dataProvider;

    @Before
    public void setUp() {
        Account account = new Account(TEST_ACCOUNT_NUMBER, new Owner(TEST_OWNER_NAME), TEST_BALANCE);
        Operation operation = new Operation(TEST_ACCOUNT_NUMBER, TEST_RECEIVER_NUMBER, TEST_BALANCE);
        List<Operation> operations = Arrays.asList(operation);
        TransferResult transferResult = new TransferResult(0, TEST_BALANCE, TEST_RECEIVER_NUMBER);

        Mockito.when(jmsReceiver.receiveMessage(eq(ACCOUNT_RESPONSE_QUEUE), any())).thenReturn(TEST_ACCOUNT_RESPONSE);
        Mockito.when(jmsReceiver.receiveMessage(eq(OPERATIONS_RESPONSE_QUEUE), any())).thenReturn(TEST_OPERATIONS_RESPONSE);
        Mockito.when(jmsReceiver.receiveMessage(eq(TRANSFER_RESPONSE_QUEUE), any())).thenReturn(TEST_TRANSFER_RESPONSE);
    }

    @Test
    public void testGetAccountInfo() {
        Account responseAccount = dataProvider.getAccountInfo(TEST_ACCOUNT_NUMBER);
        assertThat(responseAccount.getOwner().getName()).isEqualTo(TEST_OWNER_NAME);
        assertThat(responseAccount.getAccountNumber()).isEqualTo(TEST_ACCOUNT_NUMBER);
        assertThat(responseAccount.getBalance()).isEqualTo(TEST_BALANCE);
    }

    @Test
    public void testGetOperationsList() {
        List<Operation> operations = dataProvider.getOperationsList(TEST_ACCOUNT_NUMBER);
        assertThat(operations.size()).isEqualTo(2);
        assertThat(operations.get(0).getAccountFrom()).isEqualTo(TEST_ACCOUNT_NUMBER);
        assertThat(operations.get(0).getAccountTo()).isEqualTo(TEST_RECEIVER_NUMBER);
        assertThat(operations.get(0).getSumm()).isEqualTo(TEST_BALANCE);
    }

    @Test
    public void testMakeTransfer() {
        TransferRequest request = new TransferRequest(
                Integer.valueOf(TEST_ACCOUNT_NUMBER),
                Integer.valueOf(TEST_RECEIVER_NUMBER),
                TEST_BALANCE);
        TransferResult result = dataProvider.makeTransfer(request);
        assertThat(result.getStatus()).isEqualTo(0);
        assertThat(result.getAmount()).isEqualTo(TEST_BALANCE);
        assertThat(result.getAccountTarget()).isEqualTo(TEST_RECEIVER_NUMBER);
    }

    @TestConfiguration
    static class DataProviderTestContextConfiguration {
        @Bean
        public DataProvider dataProvider() {
            return new DataProvider();
        }
    }
}
