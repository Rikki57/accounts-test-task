package ru.mvbiryukov.accountsclient.integration;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mvbiryukov.accountsclient.integration.jms.JmsReceiver;
import ru.mvbiryukov.accountsclient.integration.jms.JmsSender;
import ru.mvbiryukov.accountsclient.service.dto.Account;
import ru.mvbiryukov.accountsclient.service.dto.Operation;
import ru.mvbiryukov.accountsclient.service.dto.TransferRequest;
import ru.mvbiryukov.accountsclient.service.dto.TransferResult;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class DataProvider {
    private static final String ACCOUNT_REQUEST_QUEUE="accountRequest.in";
    private static final String ACCOUNT_RESPONSE_QUEUE="accountRequest.out";
    private static final String OPERATIONS_REQUEST_QUEUE = "operationsRequest.in";
    private static final String OPERATIONS_RESPONSE_QUEUE = "operationsRequest.out";
    private static final String TRANSFER_REQUEST_QUEUE = "transferRequest.in";
    private static final String TRANSFER_RESPONSE_QUEUE = "transferRequest.out";

    @Autowired
    private JmsReceiver receiver;
    @Autowired
    private JmsSender sender;


    public Account getAccountInfo(String accountNumber){
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(ACCOUNT_REQUEST_QUEUE, accountNumber, correlationID);
        return new Gson().fromJson(
                receiver.receiveMessage(ACCOUNT_RESPONSE_QUEUE, correlationID),
                Account.class);
    }

    public List<Operation> getOperationsList(String accountNumber) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(OPERATIONS_REQUEST_QUEUE, accountNumber, correlationID);
        Type listType = new TypeToken<ArrayList<Operation>>() {
        }.getType();
        return new Gson().fromJson(
                receiver.receiveMessage(OPERATIONS_RESPONSE_QUEUE, correlationID),
                listType);
    }

    public TransferResult makeTransfer(TransferRequest request) {
        UUID correlationID = UUID.randomUUID();
        sender.sendMessage(TRANSFER_REQUEST_QUEUE, request, correlationID);
        return new Gson().fromJson(
                receiver.receiveMessage(TRANSFER_RESPONSE_QUEUE, correlationID),
                TransferResult.class);
    }
}
