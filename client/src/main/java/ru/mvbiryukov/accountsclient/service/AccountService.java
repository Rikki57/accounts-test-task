package ru.mvbiryukov.accountsclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mvbiryukov.accountsclient.integration.DataProvider;
import ru.mvbiryukov.accountsclient.service.dto.Account;
import ru.mvbiryukov.accountsclient.service.dto.Operation;
import ru.mvbiryukov.accountsclient.service.dto.TransferRequest;
import ru.mvbiryukov.accountsclient.service.dto.TransferResult;

import java.util.List;

@Service
public class AccountService {
    @Autowired
    DataProvider dataProvider;

    public Account getAccountInfo(String accountNumber){
        return dataProvider.getAccountInfo(accountNumber);
    }

    public List<Operation> getOperations(String accountNumber) {
        return dataProvider.getOperationsList(accountNumber);
    }

    public TransferResult makeTransfer(String accountNumber, String accountTarget, Double amount){
        TransferRequest request = new TransferRequest(Integer.valueOf(accountNumber), Integer.valueOf(accountTarget), amount);
        return dataProvider.makeTransfer(request);
    }
}
