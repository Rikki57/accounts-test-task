package ru.mvbiryukov.accountsserver.dao.inter;

import org.springframework.stereotype.Repository;
import ru.mvbiryukov.accountsserver.dao.exception.TransferException;
import ru.mvbiryukov.accountsserver.service.dto.Account;

@Repository
public interface AccountDao {
    Account getAccount(Integer accountNumber);

    public void increaseBalance(Integer accountNumber, Double amount);

    public void decreaseBalance(Integer accountNumber, Double amount) throws TransferException;
}
