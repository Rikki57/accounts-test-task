package ru.mvbiryukov.accountsserver.dao.inter;

import ru.mvbiryukov.accountsserver.service.dto.Operation;

import java.util.List;

public interface OperationDao {
    List<Operation> getAccountOperations(Integer accountNumber);

    void addOperation(Integer accountFrom, Integer accountTo, Double amount);
}
