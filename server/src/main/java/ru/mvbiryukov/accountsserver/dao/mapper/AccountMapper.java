package ru.mvbiryukov.accountsserver.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import ru.mvbiryukov.accountsserver.service.dto.Account;
import ru.mvbiryukov.accountsserver.service.dto.Owner;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements RowMapper<Account> {
    public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
        Account account = new Account();
        account.setAccountNumber(rs.getString("account_number"));
        account.setOwner(new Owner(rs.getString("owner")));
        account.setBalance(rs.getDouble("balance"));
        return account;
    }
}
